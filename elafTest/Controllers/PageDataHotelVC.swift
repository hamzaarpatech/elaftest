//
//  PageDataVC2.swift
//  elafTest
//
//  Created by Hamza on 3/10/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class PageDataHotelVC: UIViewController {
    
    var model : HotelData?
    @IBOutlet weak var sliderImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let sliderImgUrl = model?.featured_image {
            if let url = URL(string: sliderImgUrl) {
                sliderImage.af_setImage(withURL: url)
            }
        }
    }
}
