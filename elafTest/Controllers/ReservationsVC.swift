//
//  ReservationsVC.swift
//  elafTest
//
//  Created by Hamza on 2/28/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ReservationsVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    var reservationData : [ReservationItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.tableFooterView = UIView(frame: CGRect.zero)
        self.tableview.separatorColor = UIColor.clear
        populateData()
    }
    
    func populateData() {
        reservationData = [
           ReservationItem(image: #imageLiteral(resourceName: "thumb"), name: "Al Mashaer Hotel 1", city: "Mecca", type: "Delux Suite", inDate: "December-16-2017", outDate: "December-30-2017", id: "Booking ID:01245"),
           ReservationItem(image: #imageLiteral(resourceName: "3"), name: "Al Mashaer Hotel 2", city: "karachi", type: "Delux Suite", inDate: "December-16-2017", outDate: "December-30-2017", id: "Booking ID:01245"),
           ReservationItem(image: #imageLiteral(resourceName: "offer_image_2"), name: "Al Mashaer Hotel 3", city: "lahore", type: "Delux Suite", inDate: "December-16-2017", outDate: "December-30-2017", id: "Booking ID:01245"),
           ReservationItem(image: #imageLiteral(resourceName: "4"), name: "Al Mashaer Hotel 4", city: "quetta", type: "Delux Suite", inDate: "December-16-2017", outDate: "December-30-2017", id: "Booking ID:01245"),
           ReservationItem(image: #imageLiteral(resourceName: "offer_image"), name: "Al Mashaer Hotel 5", city: "kashmir", type: "Delux Suite", inDate: "December-16-2017", outDate: "December-30-2017", id: "Booking ID:01245")
        ]
    }
}

extension ReservationsVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reservationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "reservationCellid", for: indexPath) as? ReservationCell {
            cell.model = reservationData[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
