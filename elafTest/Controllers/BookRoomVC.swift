//
//  BookRoomVC.swift
//  elafTest
//
//  Created by Hamza on 3/5/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import SwiftyJSON

class BookRoomVC: UIViewController {
    
    var selectedCityId : Int?
    var selectedHotelId : Int?
    var hotelsArr = [PickerDisplay]()
    var passRoomData = [Translation]()
    var roomTypeImages = [HotelImages]()
    var hotelmg = [HotelData]()

    @IBOutlet weak var searchCity: CustomTextPicker!
    @IBOutlet weak var searchHotel: CustomTextPicker!
    @IBOutlet weak var pickArrivalDate: CustomDatePicker!
    @IBOutlet weak var pickDepartureDate: CustomDatePicker!
    @IBOutlet weak var selectRoomNo: CustomTextPicker!
    @IBOutlet weak var selectNoOfAdults: CustomTextPicker!
    @IBOutlet weak var selectNoOfChildren: CustomTextPicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIcons()
       
        // Fetch Cities Name
        APIClient.getCities(vc: self) { (hotels, message, error) in
            if hotels.count > 0 {
                self.searchCity.setData(data: hotels)
            }
        // Fetch Hotels Name
            self.searchCity.didSelectRow = { [weak self] (item) in
                guard let self = self else{
                    return
                }
                if let item = item {
                        let cityID = item.id
                        self.selectedCityId = cityID
                        print("Selected City id = \(cityID)")
                        let filtered = hotels.filter{ $0.id == cityID }
                        filtered.first?.hotels.forEach({ (hotelData) in
                            
                           self.hotelmg.append(hotelData)
                            
                           let translationObj = hotelData.translation.first
                           if translationObj != nil{
                            self.hotelsArr.append(translationObj!)
                            self.passRoomData.append(translationObj!)
                           }
                            let hotelImagesObj = hotelData.hotel_images.first
                            if hotelImagesObj != nil {
                                self.roomTypeImages.append(hotelImagesObj!)
                            }
                        })
                    self.searchHotel.setData(data: self.hotelsArr)
                    }
                }
            self.searchHotel.didSelectRow = { [weak self] (item) in
                if let item = item {
                    let hotelId = item.id
                    self?.selectedHotelId = hotelId
                    print("Selected Hotel Id = \(hotelId)")
                }
            }
        }
        
        selectRoomNo.setData(data: [
            RoomNumber(id: 1, name: "1"),
            RoomNumber(id: 2, name: "2"),
            RoomNumber(id: 3, name: "3"),
            RoomNumber(id: 4, name: "4")
        ])
        
        selectNoOfAdults.setData(data: [
            RoomNumber(id: 1, name: "1"),
            RoomNumber(id: 2, name: "2"),
            RoomNumber(id: 3, name: "3"),
            RoomNumber(id: 4, name: "4")
        ])
        
        selectNoOfChildren.setData(data: [
            RoomNumber(id: 1, name: "1"),
            RoomNumber(id: 2, name: "2"),
            RoomNumber(id: 3, name: "3"),
            RoomNumber(id: 4, name: "4")
        ])
        
    }
    
    func setupIcons(){
        searchCity.tintColor = UIColor.lightGray
        searchCity.setTextfieldImage(#imageLiteral(resourceName: "down-arrow"))
        searchHotel.tintColor = UIColor.lightGray
        searchHotel.setTextfieldImage(#imageLiteral(resourceName: "down-arrow"))
        pickArrivalDate.tintColor = UIColor.lightGray
        pickArrivalDate.setTextfieldImage(#imageLiteral(resourceName: "calendar"))
        pickDepartureDate.tintColor = UIColor.lightGray
        pickDepartureDate.setTextfieldImage(#imageLiteral(resourceName: "calendar"))
        selectRoomNo.tintColor = UIColor.lightGray
        selectRoomNo.setTextfieldImage(#imageLiteral(resourceName: "down-arrow"))
        selectNoOfAdults.tintColor = UIColor.lightGray
        selectNoOfAdults.setTextfieldImage(#imageLiteral(resourceName: "down-arrow"))
        selectNoOfChildren.tintColor = UIColor.lightGray
        selectNoOfChildren.setTextfieldImage(#imageLiteral(resourceName: "down-arrow"))
    }
    
    @IBAction func searchResults(_ sender: UIButton) {
        let error = validateErrors()
        if error != nil {
            Validation.alertAction(titletext: "Oops!", msgText: error!, vc: self)
        }
        else {
            if let val = searchHotel.text {
                if val.isEmpty {
                    self.performSegue(withIdentifier: "hotelListSegue", sender: nil)
                }
                else {
                    self.performSegue(withIdentifier: "roomtype", sender: nil)
                }
            }
        }
    }

    func validateErrors() -> String? {
        if let myCity = searchCity.text , let myArrival = pickArrivalDate.text , let myDeparture = pickDepartureDate.text , let roomNo = selectRoomNo.text , let adultNo = selectNoOfAdults.text , let childNo = selectNoOfChildren.text{
            if myCity.isEmpty || myArrival.isEmpty  || myDeparture.isEmpty || roomNo.isEmpty || adultNo.isEmpty || childNo.isEmpty {
                return "Please fill all the fields"
             }
         }
         return nil
     }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Goto Room Type Screen
            if segue.identifier == "roomtype" {
                if let vc = segue.destination as? RoomTypeVC {
                     for data in passRoomData {
                        vc.passName = data.name
                        vc.passDescription = data.description
                     }
                     vc.hotelImagesData = roomTypeImages
//                    vc.mainImg = hotelmg
                    }
                }
        //Goto Hotel List Screen
            if segue.identifier == "hotelListSegue" {
                if let vc = segue.destination as? HotelListVC {
                    vc.selected_city_id = selectedCityId
                    }
                }
    }
}
