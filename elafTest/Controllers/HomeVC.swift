//
//  HomeVC.swift
//   Test
//
//  Created by Hamza on 2/26/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class HomeVC: UIViewController {
   
    var pageData : [HomeSlider] = []
    @IBOutlet weak var pageContentView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        populateData()
    }
    
    func populateData() {
        APIClient.getBanners(vc: self) { ( banners, message, error) in
            if banners.count > 0 {
                self.pageData = banners
                DispatchQueue.main.async {
                    self.setupPageController()
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        pageControl.customPageControl(dotFillColor: .white, dotBorderColor: .white, dotBorderWidth: 1)
    }
     
    func setupPageController(){
        self.pageControl.numberOfPages = pageData.count
        let pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContentView.backgroundColor = .clear
        pageViewController.delegate = self
        pageViewController.dataSource = self
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        pageContentView.insertSubview(pageViewController.view, at: 0)
        
        NSLayoutConstraint.activate([
           pageViewController.view.topAnchor.constraint(equalTo: pageContentView.topAnchor),
           pageViewController.view.bottomAnchor.constraint(equalTo: pageContentView.bottomAnchor),
           pageViewController.view.leadingAnchor.constraint(equalTo: pageContentView.leadingAnchor),
           pageViewController.view.trailingAnchor.constraint(equalTo: pageContentView.trailingAnchor)
           
        ])
        
        guard let startingVC = pageDataVCat(index : Utilities.currentIndex) else {
            return
        }
        pageViewController.setViewControllers([startingVC], direction: .forward, animated: true)
    }
    
    func pageDataVCat(index : Int) -> PageDataHomeVC? {
        if index < 0 || index >= pageData.count {
            return nil
        }
        self.pageControl.currentPage = index
        guard let dataViewController = storyboard?.instantiateViewController(withIdentifier: "PageDataHomeVC") as? PageDataHomeVC else {
            return nil
        }
        
        dataViewController.model = pageData[index]
        return dataViewController
    }
}

extension HomeVC : UIPageViewControllerDataSource , UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        if Utilities.currentIndex > 0 {
            Utilities.currentIndex -= 1
            return pageDataVCat(index: Utilities.currentIndex)
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        if Utilities.currentIndex < pageData.count - 1 {
            Utilities.currentIndex += 1
            return pageDataVCat(index: Utilities.currentIndex)
        }
        return nil
    }
}
