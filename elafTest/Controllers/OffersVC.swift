//
//  OffersVC.swift
//  elafTest
//
//  Created by Hamza on 2/26/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import AlamofireImage
import SwiftyJSON

class OffersVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var offerData = [OfferItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        populateData()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.separatorColor = UIColor.clear;
    }
    
    func populateData(){
        APIClient.getPosts(vc: self) { (offeritem, message, error) in
            if offeritem.count > 0 {
             DispatchQueue.main.async {
                 for items in offeritem {
                     self.offerData.append(items)
                 }
                 self.tableView.reloadData()
             }
           }
        }
    }
}

extension OffersVC : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if let cell = tableView.dequeueReusableCell(withIdentifier: "offerCellid", for: indexPath) as? OfferCell {
             cell.model = offerData[indexPath.row]
             return cell
         }
         return UITableViewCell()
     }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
}
