//
//  CongratsVC.swift
//  elafTest
//
//  Created by Hamza on 3/19/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class CongratsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func dismissBt(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        if let tabBarController: UITabBarController =  UIApplication.shared.keyWindowInConnectedScenes?.rootViewController as? UITabBarController{
            tabBarController.selectedIndex = 3
        }
    }
}
