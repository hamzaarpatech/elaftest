//
//  PageDataVC.swift
//  elafTest
//
//  Created by Hamza on 3/2/20.
//  Copyright © 2020 Hamza. All rights reserved.
//
import UIKit

class PageDataHomeVC: UIViewController {
    
    var model : HomeSlider?
    
    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var sliderImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let sliderImgUrl = model?.sliderImageUrl {
            if let url = URL(string: sliderImgUrl) {
                sliderImage.af_setImage(withURL: url)
            }
        }
    }
}
