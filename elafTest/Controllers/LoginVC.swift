//
//  LoginVC.swift
//  elafTest
//
//  Created by Hamza on 3/17/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class LoginVC:  UIViewController {

    @IBOutlet weak var modalVIew: UIView!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var password: UITextField!
    
    var check : BookingInfoVC?
    let nc = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        modalVIew.layer.cornerRadius = 5
        modalVIew.layer.masksToBounds = true
        userEmail.delegate = self
        userEmail.returnKeyType = .next
        password.delegate = self
        password.returnKeyType = .done
    }
 
    @IBAction func proceddBtAction(_ sender: Any) {
        let error = validateErrors()
        if error != nil {
            Validation.alertAction(titletext: "Oops!", msgText: error!, vc: self)
        }
        else{
            
            if Environment.user != nil {
                Environment.user = nil
            }
            
            else {
                            //API Condition
                            if let myemail = userEmail.text , let mypass = password.text {
                                APIClient.login(vc: self, email: myemail, password: mypass) { (error, message, user) in
                                Environment.user = user
                             }
                            }
                            self.clearFields()
                            let alert = UIAlertController(title: "Congrats!", message: "You have succesfully signed in", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                self.nc.post(name: NSNotification.Name(rawValue: "refresh"), object: nil)
                                self.dismiss(animated: true, completion: nil)
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func backBtDismiss(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sigupBtAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        if let tabBarController: UITabBarController =  UIApplication.shared.keyWindowInConnectedScenes?.rootViewController as? UITabBarController{
            tabBarController.selectedIndex = 1
        }
    }
    
    func validateErrors() -> String? {
        if let email = userEmail.text , let pass = password.text {
            if email.isEmpty || pass.isEmpty {
                return "Please fill all the fields"
            }
            let error1 = Validation.checkPassRegex(passwordText: pass)
            let error2 = Validation.checkEmailRegex(emailText: email)
                       
            if error1 != nil{
                return error1
            }
            else if error2 != nil {
                return error2
            }
        }
        return nil
    }
    
    func clearFields() {
        userEmail.text = ""
        password.text = ""
    }
}

extension LoginVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.userEmail{
            textField.resignFirstResponder()
            password.becomeFirstResponder()
        }
        else if textField == self.password {
            textField.resignFirstResponder()
            Utilities.closeKeyboard(self.view)
            proceddBtAction(Any.self)
        }
        return true
    }
}
