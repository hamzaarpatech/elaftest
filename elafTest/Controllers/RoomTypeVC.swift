//
//  RoomTypeVC.swift
//  elafTest
//
//  Created by Hamza on 3/11/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class RoomTypeVC: UIViewController {

    //var model : HotelListItem!
    var model : HotelData!
    var roomData : [RoomTypeItem] = []
    var passName : String?
    var passDescription : String?
    var hotelImagesData : [HotelImages] = []
    var mainImg : [HotelData] = []
    
    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var hotelName: UILabel!
    @IBOutlet weak var hotelDescription: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
       super.viewDidLoad()
       addLeftBarIcon()
       populateData()
       tableView.tableFooterView = UIView(frame: .zero)
       self.tableView.separatorColor = UIColor.clear
        
        hotelName.text = "Book \(passName!)"
        hotelDescription.text = passDescription
   
//        if let url = URL(string: mainImg) {
//            hotelImage.af_setImage(withURL: url)
//        }
        
    }
   
    
    
    func populateData() {
   
        
    }
    
    
//    func populateData(){
//        roomData = [
//            RoomTypeItem(img: #imageLiteral(resourceName: "mountain4"), detail: "Perfect for single traveller and small families", features: "LCD , LED , high speed internet , write desk", rates: "500 SR", available: "7 Rooms"),
//        RoomTypeItem(img: #imageLiteral(resourceName: "2"), detail: "Perfect for single traveller and small families", features: "LCD , LED , high speed internet , write desk", rates: "500 SR", available: "7 Rooms"),
//        RoomTypeItem(img: #imageLiteral(resourceName: "offer_image"), detail: "Perfect for single traveller and small families", features: "LCD , LED , high speed internet , write desk", rates: "500 SR", available: "7 Rooms"),
//        RoomTypeItem(img: #imageLiteral(resourceName: "offer_image_2"), detail: "Perfect for single traveller and small families", features: "LCD , LED , high speed internet , write desk", rates: "500 SR", available: "7 Rooms"),
//        RoomTypeItem(img: #imageLiteral(resourceName: "1a"), detail: "Perfect for single traveller and small families", features: "LCD , LED , high speed internet , write desk", rates: "500 SR", available: "7 Rooms")
//        ]
//    }
}

extension RoomTypeVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotelImagesData.count
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "roomTypeId", for: indexPath) as? RoomTypeCell {
            cell.model = hotelImagesData[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let secondVC = self.storyboard?.instantiateViewController(identifier: "BookingInfoVCid") as? BookingInfoVC {
        //secondVC.model = hotelListData[indexPath.row]
        self.navigationController?.pushViewController(secondVC, animated: true)
        }
    }
}
