//
//  BookingInfoVC.swift
//  elafTest
//
//  Created by Hamza on 3/12/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class BookingInfoVC: UIViewController {
    
    @IBOutlet weak var personalInfoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bookingInfoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var specialReqHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var PayInfoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var creditBTHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var amaxBTHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sadatBTHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var visaBTHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var visaImgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var masterBTHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var masterImgHeightConstraint: NSLayoutConstraint!
    
    var activeTextField = UITextField()
    @IBOutlet weak var scrollView: UIScrollView!

    // Personal Information Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var addressTextView: UITextView!
    
    // Booking Information Outlets
    @IBOutlet weak var arrivalDate: UITextField!
    @IBOutlet weak var departureDate: UITextField!
    @IBOutlet weak var noOfRooms: UITextField!
    @IBOutlet weak var noOfAdults: UITextField!
    @IBOutlet weak var noOfChildren: UITextField!
    
    // Special Requirement Outlet
    @IBOutlet weak var specialText: UITextView!
    
    // Payment Information Outlets
    @IBOutlet weak var creditRadioBt: UIButton!
    @IBOutlet weak var amaxRadioBt: UIButton!
    @IBOutlet weak var sadatRadioBt: UIButton!
    @IBOutlet weak var visaRadioBt: UIButton!
    @IBOutlet weak var masterRadioBt: UIButton!
    @IBOutlet weak var customerName: UITextField!
    @IBOutlet weak var customerCardNo: UITextField!
    @IBOutlet weak var expiryMonth: CustomTextPicker!
    @IBOutlet weak var expiryYear: CustomTextPicker!
    @IBOutlet weak var securityCode: UITextField!
  
    var open : Bool = false
    let nc = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setDelegates()
        populatePayData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        personalInfoHeightConstraint.constant = 0
        bookingInfoHeightConstraint.constant = 0
        specialReqHeightConstraint.constant = 0
        
        PayInfoHeightConstraint.constant = 0
        creditBTHeightContraint.constant = 0
        amaxBTHeightConstraint.constant = 0
        sadatBTHeightConstraint.constant = 0
        visaBTHeightConstraint.constant = 0
        visaImgHeightConstraint.constant = 0
        masterBTHeightConstraint.constant = 0
        masterImgHeightConstraint.constant = 0

        //set personal info fields if user already registered
        nc.addObserver(self, selector: #selector(checkUserSession), name: NSNotification.Name(rawValue: "refresh"), object: nil)
        checkUserSession()
    }
    
    
    @objc func checkUserSession() {
            if let userName = Environment.user?.name , let userEmail = Environment.user?.email , let userAddress = Environment.user?.address {
                nameTextField.text = userName
                emailTextField.text = userEmail
                addressTextView.text = userAddress
        }
    }
    
    @IBAction func personalTapped(_ sender: UIButton) {
        if open == false {
            personalInfoHeightConstraint.constant = 223
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            open = true
        }
        else if open == true {
            personalInfoHeightConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            open = false
        }
    }
    
    @IBAction func bookingTapped(_ sender: UIButton) {
        if open == false{
            bookingInfoHeightConstraint.constant = 260
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            open = true
        }
        else if open == true {
            bookingInfoHeightConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            open = false
        }
    }
    
    @IBAction func specialTapped(_ sender: UIButton) {
        if open == false {
            specialReqHeightConstraint.constant = 130
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            open = true
        }
        else if open == true {
            specialReqHeightConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            open = false
        }
    }
    
    @IBAction func paymentTapped(_ sender: UIButton) {
        if open == false {
            PayInfoHeightConstraint.constant = 345
            creditBTHeightContraint.constant = 18
            amaxBTHeightConstraint.constant = 18
            sadatBTHeightConstraint.constant = 18
            visaBTHeightConstraint.constant = 18
            visaImgHeightConstraint.constant = 25
            masterBTHeightConstraint.constant = 18
            masterImgHeightConstraint.constant = 25
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            open = true
        }
        else if open == true {
            PayInfoHeightConstraint.constant = 0
            creditBTHeightContraint.constant = 0
            amaxBTHeightConstraint.constant = 0
            sadatBTHeightConstraint.constant = 0
            visaBTHeightConstraint.constant = 0
            visaImgHeightConstraint.constant = 0
            masterBTHeightConstraint.constant = 0
            masterImgHeightConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            open = false
        }
    }
    
    func setupUI() {
        addressTextView.text = Utilities.placeHolders.addressText
        addressTextView.textColor = Utilities.textViewBorder.textColor
        addressTextView.layer.borderColor = Utilities.textViewBorder.borderColor
        addressTextView.layer.borderWidth = Utilities.textViewBorder.borderWidth
        addressTextView.layer.cornerRadius = Utilities.textViewBorder.cornerRadius
        specialText.text = Utilities.placeHolders.specialText
        specialText.textColor = Utilities.textViewBorder.textColor
        specialText.layer.borderColor = Utilities.textViewBorder.borderColor
        specialText.layer.borderWidth = Utilities.textViewBorder.borderWidth
        specialText.layer.cornerRadius = Utilities.textViewBorder.cornerRadius
        Utilities.styleTextField(arrivalDate)
        Utilities.styleTextField(departureDate)
        Utilities.styleTextField(noOfRooms)
        Utilities.styleTextField(noOfAdults)
        Utilities.styleTextField(noOfChildren)
        expiryMonth.tintColor = UIColor.lightGray
       // expiryMonth.setTextfieldImage(#imageLiteral(resourceName: "updown"))
        expiryYear.tintColor = UIColor.lightGray
       // expiryYear.setTextfieldImage(#imageLiteral(resourceName: "updown"))
    }
    
    func setDelegates() {
         nameTextField.delegate = self
         nameTextField.returnKeyType = .next
         emailTextField.delegate = self
         emailTextField.returnKeyType = .next
         addressTextView.delegate = self
         specialText.delegate = self
         customerName.delegate = self
         customerName.returnKeyType = .next
         customerCardNo.delegate = self
         customerCardNo.returnKeyType = .next
         securityCode.delegate = self
         securityCode.returnKeyType = .done
     }
    
    func populatePayData() {
        expiryMonth.setData(data: [
            Month(id: 1, month: "January"),Month(id: 2, month: "February"),
            Month(id: 3, month: "March"),Month(id: 4, month: "April"),
            Month(id: 5, month: "May"),Month(id: 6, month: "June"),
            Month(id: 7, month: "July"),Month(id: 8, month: "August"),
            Month(id: 9, month: "September"),Month(id: 10, month: "October"),
            Month(id: 11, month: "Novermber"),Month(id: 12, month: "December")
        ])
        expiryYear.setData(data: [
            Year(id: 2020, year: "2020"),Year(id: 2021,year: "2021"),
            Year(id: 2022,year: "2022"),Year(id: 2023,year: "2023"),
            Year(id: 2024,year: "2024"),Year(id: 2025,year: "2025"),
            Year(id: 2026,year: "2026"),Year(id: 2027,year: "2027"),
            Year(id: 2028,year: "2028"),Year(id: 2029,year: "2029"),
            Year(id: 2030,year: "2030")
        ])
    }
    
    @IBAction func creditBtAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            amaxRadioBt.isSelected = false
            sadatRadioBt.isSelected = false
        }
        else{
            sender.isSelected = true
            amaxRadioBt.isSelected = false
            sadatRadioBt.isSelected = false
        }
    }
    
    @IBAction func amaxBtAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            creditRadioBt.isSelected = false
            sadatRadioBt.isSelected = false
        }
        else{
            sender.isSelected = true
            creditRadioBt.isSelected = false
            sadatRadioBt.isSelected = false
        }
    }
    
    @IBAction func sadatBtAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            creditRadioBt.isSelected = false
            amaxRadioBt.isSelected = false
        }
        else{
            sender.isSelected = true
            creditRadioBt.isSelected = false
            amaxRadioBt.isSelected = false
        }
    }
    
    @IBAction func visaAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            masterRadioBt.isSelected = false
        }
        else{
            sender.isSelected = true
            masterRadioBt.isSelected = false
        }
    }
    
    @IBAction func masterAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            visaRadioBt.isSelected = false
        }
        else{
            sender.isSelected = true
            visaRadioBt.isSelected = false
        }
    }
    
    @IBAction func ProceedAction(_ sender: Any) {
        let error = validateErrors()
        if error != nil {
            Validation.alertAction(titletext: "Oops!", msgText: error!, vc: self)
        }
        else{
            clearfields()
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CongratsVCid") as? CongratsVC {
                self.present(vc, animated: true, completion: nil)
                }
        }
    }
    
    func validateErrors() -> String? {
          if let myName = nameTextField.text , let myEmail = emailTextField.text , let myAddress = addressTextView.text ,
             let mySpecial = specialText.text , let creditName = customerName.text , let creditNo = customerCardNo.text ,
            let creditCode = securityCode.text , let expiryMonth = expiryMonth.text , let expiryYear = expiryYear.text {
             
             if myName.isEmpty  || myEmail.isEmpty  || myAddress.isEmpty  || mySpecial.isEmpty ||
                 creditName.isEmpty  || creditNo.isEmpty  || creditCode.isEmpty || expiryMonth.isEmpty ||
                 expiryYear.isEmpty {
                 return "Please fill all the fields"
              }
            
            let error = Validation.checkEmailRegex(emailText: myEmail)
            return error
          }
          return nil
      }
    
    func clearfields(){
        nameTextField.text = ""
        emailTextField.text = ""
        addressTextView.text = ""
        arrivalDate.text = ""
        departureDate.text = ""
        noOfRooms.text = ""
        noOfAdults.text = ""
        noOfChildren.text = ""
        customerName.text = ""
        customerCardNo.text = ""
        expiryMonth.text = ""
        expiryYear.text = ""
        securityCode.text = ""
        specialText.text = ""
        creditRadioBt.isSelected = false
        amaxRadioBt.isSelected = false
        sadatRadioBt.isSelected = false
        visaRadioBt.isSelected = false
        masterRadioBt.isSelected = false
    }
}

extension BookingInfoVC : UITextFieldDelegate , UITextViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.nameTextField {
            textField.resignFirstResponder()
            emailTextField.becomeFirstResponder()
        }
        else if textField == self.emailTextField {
            textField.resignFirstResponder()
            addressTextView.becomeFirstResponder()
            
        }
        else if textField == self.customerName {
            textField.resignFirstResponder()
            customerCardNo.becomeFirstResponder()
            
        }
        else if textField == self.customerCardNo {
            textField.resignFirstResponder()
            securityCode.becomeFirstResponder()
            
        }
        else if textField == self.securityCode {
            textField.resignFirstResponder()
            Utilities.closeKeyboard(self.view)
            ProceedAction(Any.self)
        }
        return true
    }

    @objc func keyboardWillShow(_ notification: NSNotification) {
        self.scrollView.isScrollEnabled = true
        let info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets

        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.securityCode {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }

    @objc func keyboardWillHide(_ notification: NSNotification) {
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
         if textView.textColor == UIColor.lightGray {
             textView.text = nil
             textView.textColor = UIColor.black
         }
     }
     
     func textViewDidEndEditing(_ textView: UITextView) {
         if textView.text.isEmpty{
             textView.textColor = UIColor.lightGray
            if textView == self.addressTextView{
                addressTextView.text = Utilities.placeHolders.addressText
            }
            else if textView == self.specialText{
                specialText.text = Utilities.placeHolders.specialText
            }
         }
     }
}
