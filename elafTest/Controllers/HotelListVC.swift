//
//  HotelListVC.swift
//   Test
//
//  Created by Hamza on 3/10/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class HotelListVC: UIViewController {

    var hotelListData : [HotelData] = []
    var hotelTransData : [Translation] = []
    var selected_city_id : Int?
    var pageData : [HotelData] = []
    var bannerCount : [HotelData] = []
    var passRoomData = [Translation]()
    var roomTypeImages = [HotelImages]()

    @IBOutlet weak var pageContentView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.separatorColor = UIColor.clear
        populateData()
    }
    
    func populateData() {
        APIClient.getCities(vc: self) { ( hotels, message, error) in
            if hotels.count > 0 {
                DispatchQueue.main.async {
                    let filtered = hotels.filter{ $0.id == self.selected_city_id }
                    filtered.first?.hotels.forEach({ (hotelData) in
                        self.hotelListData.append(hotelData)
                        self.bannerCount.append(hotelData)
                        let translationObj = hotelData.translation.first
                        if translationObj != nil{
                            self.hotelTransData.append(translationObj!)
                            self.passRoomData.append(translationObj!)
                        }
                        let hotelImagesObj = hotelData.hotel_images.first
                        if hotelImagesObj != nil {
                            self.roomTypeImages.append(hotelImagesObj!)
                        }
                    })
                    self.pageData = self.bannerCount
                    self.setupPageController()
                    self.tableView.reloadData()
                    }
                }
            }
        }
    
    func setupPageController(){
        self.pageControl.numberOfPages = pageData.count
        let pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContentView.backgroundColor = .clear
        pageViewController.delegate = self
        pageViewController.dataSource = self
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        pageContentView.insertSubview(pageViewController.view, at: 0)
        
        NSLayoutConstraint.activate([
           pageViewController.view.topAnchor.constraint(equalTo: pageContentView.topAnchor),
           pageViewController.view.bottomAnchor.constraint(equalTo: pageContentView.bottomAnchor),
           pageViewController.view.leadingAnchor.constraint(equalTo: pageContentView.leadingAnchor),
           pageViewController.view.trailingAnchor.constraint(equalTo: pageContentView.trailingAnchor)
           
        ])
        
        guard let startingVC = pageDataVCat(index : Utilities.currentIndex) else {
            return
        }
        pageViewController.setViewControllers([startingVC], direction: .forward, animated: true)
    }

    
    func pageDataVCat(index : Int) -> PageDataHotelVC? {
        if index < 0 || index >= pageData.count {
            return nil
        }
        self.pageControl.currentPage = index
        guard let dataViewController = storyboard?.instantiateViewController(withIdentifier: "PageDataHotelVC") as? PageDataHotelVC else {
            return nil
        }
        
        dataViewController.model = pageData[index]
        return dataViewController
    }
    
    
}

extension HotelListVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotelListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "hotelListingId", for: indexPath) as? HotelListingCell {
            cell.model = hotelListData[indexPath.row]
            cell.model2 = hotelTransData[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let secondVC = self.storyboard?.instantiateViewController(identifier: "RoomTypeVCid") as? RoomTypeVC {
            secondVC.model = hotelListData[indexPath.row]
            for data in passRoomData {
               secondVC.passName = data.name
               secondVC.passDescription = data.description
            }
            secondVC.hotelImagesData = roomTypeImages
            self.navigationController?.pushViewController(secondVC, animated: true)
        }
    }
}

extension HotelListVC : UIPageViewControllerDataSource , UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        if Utilities.currentIndex > 0 {
            Utilities.currentIndex -= 1
            return pageDataVCat(index: Utilities.currentIndex)
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        if Utilities.currentIndex < pageData.count - 1 {
            Utilities.currentIndex += 1
            return pageDataVCat(index: Utilities.currentIndex)
        }
        return nil
    }
}
