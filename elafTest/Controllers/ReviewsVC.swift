//
//  ReviewsVC.swift
//  elafTest
//
//  Created by Hamza on 2/26/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ReviewsVC: UIViewController {

    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var reviewText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        reviewText.delegate = self
        reviewText.returnKeyType = .done
        reviewText.text = Utilities.placeHolders.reviewText
        reviewText.textColor = Utilities.textViewBorder.textColor
        reviewText.layer.borderColor = Utilities.textViewBorder.borderColor
        reviewText.layer.borderWidth = Utilities.textViewBorder.borderWidth
        reviewText.layer.cornerRadius = Utilities.textViewBorder.cornerRadius
    }
    
    
    @IBAction func submitAction(_ sender: Any) {
        let error = validation()
        if error != nil {
            Validation.alertAction(titletext: "Oops!", msgText: error!, vc: self)
        }
        else{
            Validation.alertAction(titletext: "Thank You", msgText: "for your review and rating", vc: self)
            reviewText.text = ""
            
        }
    }
    
    func validation() -> String?{
        if let submitText = reviewText.text  {
            if submitText.isEmpty || submitText == Utilities.placeHolders.reviewText {
                return "Please write review text"
            }
        }
        return nil
    }
}

extension ReviewsVC : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == reviewText {
            if text == "\n" {
                reviewText.becomeFirstResponder()
                Utilities.closeKeyboard(self.view)
                submitAction(Any.self)
            }
        }
        return true
    }
        
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty{
            textView.textColor = UIColor.lightGray
           if textView == self.reviewText{
               reviewText.text = Utilities.placeHolders.addressText
           }
        }
    }
}
