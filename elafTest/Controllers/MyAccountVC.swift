//
//  MyAccountView.swift
//  elafTest
//
//  Created by Hamza on 3/3/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class MyAccountVC: UIViewController {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var userAddress: UITextView!
    @IBOutlet weak var userPhone: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        Utilities.closeKeyboard(self.view)
    }
    
    @IBAction func RegisterBt(_ sender: Any) {
        let error = validateErrors()
        if error != nil {
            Validation.alertAction(titletext: "Oops!", msgText: error!, vc: self)
        }
        else{
           //API Condition
            if let myname = userName.text , let myemail = userEmail.text , let mypass = userPassword.text , let myaddress = userAddress.text , let myphone = userPhone.text{
                
                APIClient.register(vc: self, name:myname , email:myemail , password:mypass , address:myaddress , phone:myphone ) { (error, message, user) in
                    if user != nil {
                        APIClient.login(vc: self, email: myemail, password: mypass) { (error, message, user) in
                                                        Environment.user = user
                                                        Validation.alertAction(titletext: "Congrats!", msgText: "You have succesfully signed up", vc: self)
                                                        self.clearFields()
                                                    }
                    }
                    else {
                      Validation.alertAction(titletext: "Oops!", msgText: "The email has already been taken.", vc: self)
                    }
                }
            }
        }
    }
    
    func validateErrors() -> String? {
        if let myname = userName.text , let myemail = userEmail.text , let mypass = userPassword.text , let myaddress = userAddress.text , let myphone = userPhone.text{
            if myname.isEmpty  || myemail.isEmpty  || mypass.isEmpty  || myaddress.isEmpty || myphone.isEmpty {
               return "Please fill all the fields"
            }
            
            let error1 = Validation.checkPassRegex(passwordText: mypass)
            let error2 = Validation.checkEmailRegex(emailText: myemail)
            
            if error1 != nil{
                return error1
            }
            else if error2 != nil {
                return error2
            }
        }
        return nil
    }
    
    func setupUI() {
        userName.delegate = self
        userEmail.delegate = self
        userPassword.delegate = self
        userPhone.delegate = self
        userAddress.delegate = self
        userName.returnKeyType = .next
        userEmail.returnKeyType = .next
        userPassword.returnKeyType = .next
        userPhone.returnKeyType = .next
        userAddress.returnKeyType = .done
        
        userAddress.text = Utilities.placeHolders.addressText
        userAddress.textColor = Utilities.textViewBorder.textColor
        userAddress.layer.borderColor = Utilities.textViewBorder.borderColor
        userAddress.layer.borderWidth = Utilities.textViewBorder.borderWidth
        userAddress.layer.cornerRadius = Utilities.textViewBorder.cornerRadius
        }
    
    func clearFields(){
         userName.text = ""
         userEmail.text = ""
         userPassword.text = ""
         userAddress.text = ""
         userPhone.text = ""
    }
}

extension MyAccountVC: UITextFieldDelegate , UITextViewDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          if textField == self.userName {
             textField.resignFirstResponder()
             userEmail.becomeFirstResponder()
          } else if textField == userEmail {
             textField.resignFirstResponder()
             userPassword.becomeFirstResponder()
          } else if textField == userPassword {
             textField.resignFirstResponder()
             userPhone.becomeFirstResponder()
          } else if textField == userPhone {
             textField.resignFirstResponder()
             userAddress.becomeFirstResponder()
          }
         return true
        }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        if textView == userAddress{
           if text == "\n" {
                textView.resignFirstResponder()
                Utilities.closeKeyboard(self.view)
                RegisterBt(Any.self)
           }
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty{
            textView.text = Utilities.placeHolders.addressText
            textView.textColor = UIColor.lightGray
        }
    }
}
