//
//  DiscoverVC.swift
//  elafTest
//
//  Created by Hamza on 2/28/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class DiscoverVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    var discoverData : [DiscoverItem] = []
    var spaces = "    "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateData()
        tableview.tableFooterView = UIView(frame: CGRect.zero)
        self.tableview.separatorColor = UIColor.clear
    }
    
    func populateData(){
        discoverData = [
            DiscoverItem(heading: "\(spaces)Family Tour" , description: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. " , image: #imageLiteral(resourceName: "offer_image")) ,
            DiscoverItem(heading: "\(spaces)Business Tour" , description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda." , image: #imageLiteral(resourceName: "2")) ,
            DiscoverItem(heading: "\(spaces)Vaccation Tour" , description: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." , image: #imageLiteral(resourceName: "offer_image_2")) ,
            DiscoverItem(heading: "\(spaces)Study Tour" , description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda." , image: #imageLiteral(resourceName: "5"))
       ]
    }
}

extension DiscoverVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoverData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "discoverCellid", for: indexPath) as? DiscoverCell {
            cell.model = discoverData[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
