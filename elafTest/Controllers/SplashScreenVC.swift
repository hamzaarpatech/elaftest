//
//  SplashScreenVC.swift
//  elafTest
//
//  Created by Hamza on 2/26/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import Alamofire

class SplashScreenVC: UIViewController {

    @IBOutlet weak var gettingStarted: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gettingStarted.isHidden = true
    }

    @IBAction func gettingStarted(_ sender: UIButton) {
        guard let window = UIApplication.shared.keyWindowInConnectedScenes else {return}
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let TabbarController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarId") as! UITabBarController
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion: nil )
        UIApplication.shared.keyWindowInConnectedScenes?.rootViewController = TabbarController
    }
    
    @IBAction func selectLanguage(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Language", preferredStyle: .actionSheet)
        APIClient.getLanguages(vc: self) { ( languages, message, error) in
           if languages.count > 0 {
            for language in languages {
                let action = UIAlertAction(title: language.name, style: .default) { (action) in
                    print(language.code)
                    UserDefaults.standard.set(language.code, forKey: "code")
                }
                optionMenu.addAction(action)
            }
            self.present(optionMenu, animated: true) {
                self.gettingStarted.isHidden = false
            }
           }
        }
    }
}
