//
//  DiscoverCellVC.swift
//  elafTest
//
//  Created by Hamza on 3/2/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class DiscoverCell: UITableViewCell {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descriptionImage: UIImageView!
    @IBOutlet weak var descriptionText: UILabel!
    
    override func layoutSubviews() {
           super.layoutSubviews()
           self.descriptionImage.layer.cornerRadius = Utilities.imageFrame.cornerRadius
           contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: Utilities.imageFrame.top,
                                                                        left: Utilities.imageFrame.left,
                                                                        bottom: Utilities.imageFrame.bottom,
                                                                        right: Utilities.imageFrame.right))
    }
    
    var model : DiscoverItem! {
        didSet{
            headingLabel.text = model.headingLabel
            descriptionText.text = model.descriptionText
            descriptionImage.image = model.descriptionImage
        }
    }
    
    @IBAction func loadMoreBT(_ sender: UIButton) {
    }
}
