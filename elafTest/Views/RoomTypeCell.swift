//
//  RoomTypeCell.swift
//  elafTest
//
//  Created by Hamza on 3/11/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class RoomTypeCell: UITableViewCell {

    @IBOutlet weak var roomImage: UIImageView!
    @IBOutlet weak var roomDetail: UILabel!
    @IBOutlet weak var roomFeatures: UILabel!
    @IBOutlet weak var roomRates: UILabel!
    @IBOutlet weak var roomAvailable: UILabel!
    
//    var model : RoomTypeItem! {
//           didSet{
//               roomImage.image = model.hotelImage
//               roomDetail.text = model.roomDetail
//               roomFeatures.text = model.roomFeatures
//               roomRates.text = model.roomRates
//               roomAvailable.text = model.roomAvailable
//           }
//       }
    var model : HotelImages! {
           didSet{
                if let url = URL(string: model.image) {
                    roomImage.af_setImage(withURL: url)
                }
           }
       }

}
