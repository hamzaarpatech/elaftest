//
//  TableviewCellsVC.swift
//  elafTest
//
//  Created by Hamza on 2/28/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ReservationCellsVC: UITableViewCell {
  
    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var hotelName: UILabel!
    @IBOutlet weak var hotelCity: UILabel!
    @IBOutlet weak var hotelType: UILabel!
    @IBOutlet weak var checkInDate: UILabel!
    @IBOutlet weak var checkOutDate: UILabel!
    @IBOutlet weak var bookingID: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.hotelImage.layer.cornerRadius =  5
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0))
    }
    
    var model : ReservationItem! {
        didSet{
            hotelImage.image = model.hotelImage
            hotelName.text = model.hotelName
            hotelCity.text = model.hotelCity
            hotelType.text = model.hotelType
            checkInDate.text = model.checkInDate
            checkOutDate.text = model.checkOutDate
        }
    }
}
