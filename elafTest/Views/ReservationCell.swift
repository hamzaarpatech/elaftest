//
//  ReservationCellVC.swift
//  elafTest
//
//  Created by Hamza on 3/2/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ReservationCell: UITableViewCell {
  
    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var hotelName: UILabel!
    @IBOutlet weak var hotelCity: UILabel!
    @IBOutlet weak var hotelType: UILabel!
    @IBOutlet weak var checkInDate: UILabel!
    @IBOutlet weak var checkOutDate: UILabel!
    @IBOutlet weak var bookingID: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.hotelImage.layer.cornerRadius =  Utilities.imageFrame.cornerRadius
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: Utilities.imageFrame.top,
                                                                     left: Utilities.imageFrame.left,
                                                                     bottom: Utilities.imageFrame.bottom,
                                                                     right: Utilities.imageFrame.right))
    }
    
    var model : ReservationItem! {
        didSet{
            hotelImage.image = model.hotelImage
            hotelName.text = model.hotelName
            hotelCity.text = model.hotelCity
            hotelType.text = model.hotelType
            checkInDate.text = model.checkInDate
            checkOutDate.text = model.checkOutDate
            bookingID.text = model.bookingID
        }
    }
}
