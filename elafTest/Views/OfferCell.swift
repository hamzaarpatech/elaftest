//
//  OfferCell.swift
//  elafTest
//
//  Created by Hamza on 3/5/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class OfferCell: UITableViewCell {
    
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var offerLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.offerImage.layer.cornerRadius = Utilities.imageFrame.cornerRadius
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: Utilities.imageFrame.top,
                                                                     left: Utilities.imageFrame.left,
                                                                     bottom: Utilities.imageFrame.bottom,
                                                                     right: Utilities.imageFrame.right))
    }
    
    var model : OfferItem! {
        didSet{
            if let url = URL(string: model.offerImage) {
              offerImage.af_setImage(withURL: url)
            }
            offerLabel.text = model.offerTitle
        }
    }
}
