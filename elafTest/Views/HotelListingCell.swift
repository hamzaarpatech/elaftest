//
//  HotelListingCell.swift
//  elafTest
//
//  Created by Hamza on 3/10/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class HotelListingCell: UITableViewCell {
    
    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var hotelName: UILabel!
    @IBOutlet weak var ratingNumber: UILabel!
    @IBOutlet weak var hotelAddress: UILabel!
    @IBOutlet weak var hotelPhone: UILabel!
    
    var model : HotelData! {
        didSet{
            if let url = URL(string: model.icon) {
              hotelImage.af_setImage(withURL: url)
            }
            hotelPhone.text = model.phone
        }
    }
    var model2 : Translation! {
        didSet{
            hotelName.text = model2.name
            hotelAddress.text = model2.address
        }
    }
}
