//
//  CustomTextPicker.swift
//  elafTest
//
//  Created by Hamza on 3/5/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol PickerDisplay {
    var name: String { get }
    var id : Int { get }
}

class CustomTextPicker: UITextField, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    let pickerView = UIPickerView()
    var data = [PickerDisplay]()
    var selectedData: PickerDisplay?
    var didSelectRow : ((PickerDisplay?) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.delegate = self
        self.inputView = pickerView
        pickerView.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let donebutton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endEditAction))
        toolBar.setItems([donebutton], animated: true)
        self.inputAccessoryView = toolBar
    }
    
    func setData(data: [PickerDisplay]) {
        self.data = data
    }

    @objc func endEditAction() {
        self.endEditing(true)
        
        // predefined first row when user select one row
        if selectedData == nil {
            self.selectedData = self.data.first
            self.text = selectedData?.name
            self.didSelectRow?(selectedData)
        } else {
            self.text = selectedData?.name
            self.didSelectRow?(selectedData)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedData = data[row]
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = selectedData?.name
    }
}
