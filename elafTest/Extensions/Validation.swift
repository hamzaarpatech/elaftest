//
//  Validation.swift
//  elafTest
//
//  Created by Hamza on 3/18/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit

struct Validation {
    
    static func checkEmailRegex(emailText:String) -> String? {
        if Utilities.isEmailValid(emailText) == false {
            return "Type Valid Email"
           }
        return nil
    }
    
    static func checkPassRegex(passwordText:String) -> String? {
        if Utilities.isPasswordValid(passwordText) == false {
         return "Type Strong Password"
        }
        return nil
    }
    
    static func alertAction(titletext:String , msgText:String , vc:UIViewController ) {
        let alert = UIAlertController(title: titletext, message: msgText, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
}
