//
//  CustomDatePicker.swift
//  elafTest
//
//  Created by Hamza on 3/9/20.
//  Copyright © 2020 Hamza. All rights reserved.
//


import UIKit

class CustomDatePicker : UITextField, UITextFieldDelegate {
   
    let datePicker = UIDatePicker()
    let formatter = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        formatter.dateFormat = "dd/MM/yyyy"
        datePicker.datePickerMode = .date
        
        self.delegate = self
        self.inputView = datePicker
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endEditAction));
        toolbar.setItems([doneButton], animated: true)
        self.inputAccessoryView = toolbar
    }
    
    @objc func endEditAction() {
        self.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = formatter.string(from: datePicker.date)
    }
}
