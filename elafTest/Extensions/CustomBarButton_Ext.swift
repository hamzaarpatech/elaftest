//
//  CustomBarButton.swift
//  elafTest
//
//  Created by Hamza on 3/11/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit

extension RoomTypeVC {
    func addLeftBarIcon() {
             let logoImage = #imageLiteral(resourceName: "logo_trans")
             let logoImageView = UIImageView.init(image: logoImage)
             logoImageView.frame = CGRect(x:0.0,y:0.0, width:80,height:35.0)
             logoImageView.contentMode = .scaleAspectFit
             let imageItem = UIBarButtonItem.init(customView: logoImageView)
             let widthConstraint = logoImageView.widthAnchor.constraint(equalToConstant: 80)
             let heightConstraint = logoImageView.heightAnchor.constraint(equalToConstant: 35)
             heightConstraint.isActive = true
             widthConstraint.isActive = true
             navigationItem.rightBarButtonItem =  imageItem
         }
}
