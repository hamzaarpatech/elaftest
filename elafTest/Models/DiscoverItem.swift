//
//  DiscoverItem.swift
//  elafTest
//
//  Created by Hamza on 3/2/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit

class DiscoverItem {

    let headingLabel : String
    let descriptionText : String
    let descriptionImage : UIImage
    
    init(heading:String , description:String , image:UIImage) {
        self.headingLabel = heading
        self.descriptionText = description
        self.descriptionImage = image
    }
}
