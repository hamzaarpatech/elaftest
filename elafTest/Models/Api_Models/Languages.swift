//
//  Languages.swift
//  elafTest
//
//  Created by Apple on 4/2/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import SwiftyJSON

class Langauges {
    var id : Int
    var name : String
    var code : String

    init(json: JSON) {
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.code = json["code"].stringValue
    }
}
