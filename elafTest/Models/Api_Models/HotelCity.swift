//
//  HotelCity.swift
//  elafTest
//
//  Created by Apple on 4/6/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import SwiftyJSON

class HotelCity : PickerDisplay {
    var id : Int
    var code : String
    var name : String
   var hotels : [HotelData]
    
    init(json : JSON) {
        self.id = json["id"].intValue
        self.code = json["code"].stringValue
        self.name = json["name"].stringValue
        self.hotels = json["hotels"].arrayValue.map({ HotelData(json: $0) })
    }
}
