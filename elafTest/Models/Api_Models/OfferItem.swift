//
//  OfferItem.swift
//  elafTest
//
//  Created by Apple on 4/14/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class OfferItem {
    
    var offerTitle : String
    var offerImage : String
    
    init(json : JSON) {
      self.offerTitle = json["title"].stringValue
      self.offerImage = json["image"].stringValue
    }
}
