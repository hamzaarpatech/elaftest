//
//  PageContentModel.swift
//  elafTest
//
//  Created by Hamza on 3/2/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class HomeSlider {
    
    var sliderImageUrl : String
    
    init(json: JSON) {
        self.sliderImageUrl = json["image"].stringValue
    }
}
