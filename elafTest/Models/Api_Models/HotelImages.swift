//
//  HotelImages.swift
//  elafTest
//
//  Created by Apple on 4/6/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import SwiftyJSON

class HotelImages {
    var image : String
    var is_featured : Int
    var hotel_id : Int
    
    init(json : JSON) {
        self.image = json["image"].stringValue
        self.is_featured = json["is_featured"].intValue
        self.hotel_id = json["hotel_id"].intValue
    }
}
