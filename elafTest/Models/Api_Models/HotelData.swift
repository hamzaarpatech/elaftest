//
//  HotelData.swift
//  elafTest
//
//  Created by Apple on 4/6/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import SwiftyJSON

class HotelData {
    var id : Int
    var hotel_code : Int
    var featured_image : String
    var icon : String
    var phone : String
    var latitude : String
    var longitude : String
    var city_id : Int
    var hotel_images : [HotelImages]
    var translation : [Translation]
    
    init(json : JSON) {
        self.id = json["id"].intValue
        self.hotel_code = json["hotel_code"].intValue
        self.featured_image = json["featured_image"].stringValue
        self.icon = json["icon"].stringValue
        self.phone = json["phone"].stringValue
        self.latitude = json["latitude"].stringValue
        self.longitude = json["longitude"].stringValue
        self.city_id = json["city_id"].intValue
        self.hotel_images = json["hotel_images"].arrayValue.map({ HotelImages(json: $0) })
        self.translation = json["translation"].arrayValue.map({ Translation(json: $0) })
    }
}
