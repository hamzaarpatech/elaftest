//
//  Translation.swift
//  elafTest
//
//  Created by Apple on 4/6/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import SwiftyJSON

class Translation : PickerDisplay {
    
    var name : String
    var description : String
    var address : String
    var id : Int
    
    init(json : JSON) {
        self.name = json["name"].stringValue
        self.description = json["description"].stringValue
        self.address = json["address"].stringValue
        self.id = json["hotel_id"].intValue
    }
}
