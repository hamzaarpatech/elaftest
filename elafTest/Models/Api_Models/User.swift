//
//  User.swift
//  elafTest
//
//  Created by Apple on 4/7/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {
    
    var id : Int
    var name : String
    var email : String
    var address : String
    var phone : String
    var status : Int
    var type : String
    var created_by : Int
    var updated_at : String
    var created_at : String
    var password : String
             
    init(json : JSON) {
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.email = json["email"].stringValue
        self.address = json["address"].stringValue
        self.phone = json["phone"].stringValue
        self.status = json["status"].intValue
        self.type = json["type"].stringValue
        self.created_by = json["created_by"].intValue
        self.updated_at = json["updated_at"].stringValue
        self.created_at = json["created_at"].stringValue
        self.password = json["password"].stringValue
    }
}
