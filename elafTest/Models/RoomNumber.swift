//
//  RoomNumber.swift
//  elafTest
//
//  Created by Faizan Naseem on 10/03/2020.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation

class RoomNumber: PickerDisplay {
    
    var id : Int
    var name: String

    
    init(id : Int , name: String) {
       self.id = id
       self.name = name
    }
}
