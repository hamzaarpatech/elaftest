//
//  ReservationItem.swift
//  elafTest
//
//  Created by Hamza on 3/2/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit

class ReservationItem {
    
    let hotelImage : UIImage
    let hotelName : String
    let hotelCity : String
    let hotelType : String
    let checkInDate : String
    let checkOutDate : String
    let bookingID : String
    
    init(image:UIImage , name:String , city:String , type:String , inDate:String , outDate:String , id:String) {
        self.hotelImage = image
        self.hotelName = name
        self.hotelCity = city
        self.hotelType = type
        self.checkInDate = inDate
        self.checkOutDate = outDate
        self.bookingID = id
    }
}
