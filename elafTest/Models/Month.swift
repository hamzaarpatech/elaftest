//
//  Month.swift
//  elafTest
//
//  Created by Hamza on 3/18/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation

class Month : PickerDisplay {
    var id : Int
    var name : String
    
    init(id:Int , month:String) {
        self.id = id
        self.name = month
    }
}
