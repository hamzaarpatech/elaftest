//
//  Year.swift
//  elafTest
//
//  Created by Hamza on 3/18/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation

class Year : PickerDisplay {
    
    var id : Int
    var name : String
    
    init(id : Int , year:String) {
        self.id = id
        self.name = year
    }
}
