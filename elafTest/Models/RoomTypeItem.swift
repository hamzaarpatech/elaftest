//
//  RoomTypeItem.swift
//  elafTest
//
//  Created by Hamza on 3/11/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit

class RoomTypeItem{
    let hotelImage : UIImage
    let roomDetail : String
    let roomFeatures : String
    let roomRates : String
    let roomAvailable : String
    
    init(img:UIImage , detail:String , features:String , rates:String , available:String ) {
        self.hotelImage = img
        self.roomDetail = detail
        self.roomFeatures = features
        self.roomRates = rates
        self.roomAvailable = available
    }
}
