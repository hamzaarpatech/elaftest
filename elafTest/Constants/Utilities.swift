//
//  Utilities.swift
//  elafTest
//
//  Created by Hamza on 3/2/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit

struct Utilities {
    
    static var currentIndex : Int = 0
  
    struct imageFrame{
        static let cornerRadius : CGFloat = 5.0
        static let top : CGFloat = 0
        static let left : CGFloat = 0
        static let bottom : CGFloat = 10
        static let right : CGFloat = 0
    }
    
    static func styleTextField(_ textfield:UITextField){
           let bottomLine = CALayer()
           bottomLine.frame = CGRect(x: 0, y: textfield.frame.height , width: textfield.frame.width, height: 1)
           bottomLine.backgroundColor = UIColor.init(red: 182/255, green: 137/255, blue: 87/255, alpha: 1).cgColor
           textfield.borderStyle = .none
           textfield.layer.addSublayer(bottomLine)
       }
    
    struct textViewBorder{
//      static let placeHolder = "Enter Address"
        static let textColor = UIColor.lightGray
        static let borderColor = UIColor.lightGray.cgColor
        static let borderWidth : CGFloat = 0.5
        static let cornerRadius : CGFloat = 5.0
    }
    
    struct placeHolders{
        static let addressText = "Enter Address"
        static let specialText = "Please Enter Your Message Here"
        static let reviewText = "Your Review"
    }
    
   static func closeKeyboard(_ view:UIView){
        let gesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(gesture)
       }
    
   static func isPasswordValid(_ password:String) -> Bool{
             let passwordRegex = "^(?=.*[a-z])(?=.*[0-9]).{8,}$"
             let passwordText = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
             return passwordText.evaluate(with: password)
         }
         
   static func isEmailValid(_ email:String) -> Bool {
             let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}$"
             let emailText = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
             return emailText.evaluate(with: email)
         }
}
