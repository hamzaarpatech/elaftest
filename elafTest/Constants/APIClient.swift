//
//  APIClient.swift
//  foruneats-ios
//
//  Created by Faizan Naseem on 08/10/2018.
//  Copyright © 2018 Hassan Ahmed Khan. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIClient {
   
    private static var headers: HTTPHeaders {
           return ["Authorization": "Bearer \(Environment.apiToken)",
               "Accept": "application/json"]
       }
    
    private static let code = UserDefaults.standard.string(forKey: "code")
    private static let langauagePara = ["language": code]
    private static let offerParam = ["language": "en", "post_type": "offer"]
    
    class func getLanguages(vc:UIViewController ,callback:@escaping (_ lang: [Langauges], _ message: String?, _ error: Error?)->Void) {
        let endpoint = "languages"
        let url = "\(Environment.baseURL)/\(endpoint)"
    
    AF.request(url ,headers: APIClient.headers).responseJSON { (response) in
        var lang = [Langauges]()
        var error: Error?
        var message: String?

        switch response.result {
         case .success(let value):
                if let code = response.response?.statusCode {
                    if code == 200 {
                        if let data = response.data, let result = try? JSON(data: data) {
                            let success = result["success"].boolValue
                            message = result["message"].stringValue
                            if success {
                                    let json = JSON(value)
                                    let data = json["data"].arrayValue
                                    for langs in data {
                                    lang.append(Langauges(json: JSON(langs)))
                                }
                            }
                            callback(lang, message, error)
                        }
                    }
                    else {
                        if let data = response.data, let result = try? JSON(data: data) {
                            message = result["message"].stringValue
                            callback(lang, message, error)
                        }
                    }
                }
          case .failure(_):
                       Validation.alertAction(titletext: "Oops", msgText: "Check Internet Connectivity", vc: vc)
                       error = response.error
                       print("Error: \(String(describing: response.error?.localizedDescription))")
                       callback(lang, message, error)
            }
        }
    }
    
    class func getBanners(vc:UIViewController ,callback:@escaping (_ ban: [HomeSlider], _ message: String?, _ error: Error?)->Void) {
            let endpoint = "banners"
            let url = "\(Environment.baseURL)/\(endpoint)"
        
        AF.request(url , method: .post , parameters: APIClient.langauagePara , headers: APIClient.headers).responseJSON { (response) in
            var banner = [HomeSlider]()
            var error: Error?
            var message: String?

            switch response.result {
             case .success(let value):
                    if let code = response.response?.statusCode {
                        if code == 200 {
                            if let data = response.data, let result = try? JSON(data: data) {
                                
                                let success = result["success"].boolValue
                                message = result["message"].stringValue
                                if success {
                                    let json = JSON(value)
                                    let data = json["data"].arrayValue
                                    for banImage in data {
                                        banner.append(HomeSlider(json: JSON(banImage)))
                                    }
                                }
                                callback(banner, message, error)
                            }
                        }
                        else {
                            if let data = response.data, let result = try? JSON(data: data) {
                                message = result["message"].stringValue
                                callback(banner, message, error)
                            }
                        }
                    }
              case .failure(_):
                           Validation.alertAction(titletext: "Oops", msgText: "Check Internet Connectivity", vc: vc)
                           print("not getting value from api")
                           error = response.error
                           print("Error: \(String(describing: response.error?.localizedDescription))")
                           callback(banner, message, error)
                }
            }
        }
  
    
    class func getCities(vc:UIViewController ,callback:@escaping (_ lang: [HotelCity], _ message: String?, _ error: Error?)->Void) {
        let endpoint = "hotels"
        let url = "\(Environment.baseURL)/\(endpoint)"
    
        AF.request(url , method: .post , parameters: langauagePara , headers: APIClient.headers).responseJSON { (response) in
        var hotelcity = [HotelCity]()
        var error: Error?
        var message: String?
        
        switch response.result {
         case .success(let value):
                if let code = response.response?.statusCode {
                    if code == 200 {
                        if let data = response.data, let result = try? JSON(data: data) {
                            let success = result["success"].boolValue
                            message = result["message"].stringValue
                            if success {
                                let json = JSON(value)
                                let data = json["data"].arrayValue
                                for postData in data {
                                hotelcity.append(HotelCity(json: JSON(postData)))
                                }
                            }
                            callback(hotelcity, message, error)
                        }
                    }
                    else {
                        if let data = response.data, let result = try? JSON(data: data) {
                            message = result["message"].stringValue
                            callback(hotelcity, message, error)
                        }
                    }
                }
          case .failure(_):
                       Validation.alertAction(titletext: "Oops", msgText: "Check Internet Connectivity", vc: vc)
                       error = response.error
                       print("Error: \(String(describing: response.error?.localizedDescription))")
                       callback(hotelcity, message, error)
            }
        }
    }
    
        
    class func register(vc:UIViewController , name:String, email:String, password:String, address:String, phone:String, block: @escaping ((_ error: Error?,_ message: String?,_ user: User?) -> Void)) {
        
        let endpoint = "myaccount"
        let url = "\(Environment.baseURL)/\(endpoint)"
        let registerUserParam: Parameters = ["name":name , "email":email , "password":password , "address":address ,"phone":phone]
        
        AF.request(url, method: .post, parameters: registerUserParam).responseJSON { (response) in
            var error: Error?
            var message: String?
            var user : User?
            
            switch response.result {
            case .success(let value):
                if let code = response.response?.statusCode {
                    if code == 200 {
                        if let data = response.data, let result = try? JSON(data: data) {
                            let success = result["success"].boolValue
                            message = result["message"].stringValue
                            if success {
                                let json = JSON(value)
                                let data = json["data"].arrayValue
                                if let userJson = data.first {
                                    user = User(json: userJson)
                                }
                                block(error,message,user)
                            }
                        }
                    }
                    else {
                        if let data = response.data, let result = try? JSON(data: data) {
                            message = result["errors"]["email"].arrayValue.first?.stringValue
                            block(error,message,user)
                        }
                    }
                }
           
            case .failure(_):
                Validation.alertAction(titletext: "Oops", msgText: "Check Internet Connectivity", vc: vc)
                error = response.error
                print("Error: \(String(describing: response.error?.localizedDescription))")
                block(error,message,user)
            }
        }
    }
    
    
    class func login(vc:UIViewController, email:String, password:String , block: @escaping ((_ error: Error?,_ message: String?,_ user: User?) -> Void)) {
           
           let endpoint = "login"
           let url = "\(Environment.baseURL)/\(endpoint)"
           let loginParam: Parameters = ["email":email , "password":password ]
           
           AF.request(url, method: .post, parameters: loginParam).responseJSON { (response) in
               var error: Error?
               var message: String?
               var user : User?
            
               switch response.result {
               case .success(let value):
                   if let code = response.response?.statusCode {
                       if code == 200 {
                           if let data = response.data, let result = try? JSON(data: data) {
                               let success = result["success"].boolValue
                               message = result["message"].stringValue
                               if success {
                                   let json = JSON(value)
                                   let data = json["data"].arrayValue
                                   if let userJson = data.first {
                                       user = User(json: userJson)
                                   }
                                   block(error,message,user)
                               }
                           }
                       }
                       else {
                           if let data = response.data, let result = try? JSON(data: data) {
                               message = result["message"].stringValue
                               block(error,message,user)
                           }
                       }
                   }
              
               case .failure(_):
                   Validation.alertAction(titletext: "Oops", msgText: "Check Internet Connectivity", vc: vc)
                   error = response.error
                   print("Error: \(String(describing: response.error?.localizedDescription))")
                   block(error,message,user)
               }
           }
       }
    
    
        class func getPosts(vc:UIViewController, callback:@escaping (_ offer: [OfferItem], _ message: String?, _ error: Error?)->Void) {
                let endpoint = "posts"
                let url = "\(Environment.baseURL)/\(endpoint)"
    
            AF.request(url , method: .post , parameters: APIClient.offerParam , headers: APIClient.headers).responseJSON { (response) in
                var post = [OfferItem]()
                var error: Error?
                var message: String?
    
                switch response.result {
                 case .success(let value):
                        if let code = response.response?.statusCode {
                            if code == 200 {
                                if let data = response.data, let result = try? JSON(data: data) {
                                    let success = result["success"].boolValue
                                    message = result["message"].stringValue
                                    if success {
                                        let json = JSON(value)
                                        let data = json["data"].arrayValue
                                        for postData in data {
                                        post.append(OfferItem(json: JSON(postData)))
                                        }
                                    }
                                    callback(post, message, error)
                                }
                            }
                            else {
                                if let data = response.data, let result = try? JSON(data: data) {
                                    message = result["message"].stringValue
                                    callback(post, message, error)
                                }
                            }
                        }
                  case .failure(_):
                               Validation.alertAction(titletext: "Oops", msgText: "Check Internet Connectivity", vc: vc)
                               error = response.error
                               print("Error: \(String(describing: response.error?.localizedDescription))")
                               callback(post, message, error)
                    }
                }
            }
}
