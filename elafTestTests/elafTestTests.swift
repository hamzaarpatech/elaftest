//
//  elafTestTests.swift
//  elafTestTests
//
//  Created by Faizan Naseem on 02/04/2020.
//  Copyright © 2020 Hamza. All rights reserved.
//

import XCTest

class elafTestTests: XCTestCase {
    
    let timeout = 35.0

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testGetLanguages() {
        let expectation = self.expectation(description: "GetLanguages")
        
        APIClient.getLanguages { (languages, message, error) in
            
            if let checkMsg = message  {
                print(checkMsg)
            }
            XCTAssertGreaterThan(languages.count, 0)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: timeout)
    }
    
    func testGetBanners() {
        let expectation = self.expectation(description: "GetBanners")
        
        APIClient.getBanners { (banners, message, error) in
            
            if let checkMsg = message  {
                print(checkMsg)
            }
            XCTAssertGreaterThan(banners.count, 0)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: timeout)
    }
    
    func testGetCities() {
        let expectation = self.expectation(description: "GetCities")
        
        APIClient.getCities { (cities, message, error) in
            
            if let checkMsg = message  {
                print(checkMsg)
            }
            XCTAssertGreaterThan(cities.count, 0)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: timeout)
    }
    
    func testGetPosts() {
        let expectation = self.expectation(description: "GetPosts")
        
        APIClient.getPosts { (posts, message, error) in
            
            if let checkMsg = message  {
                print(checkMsg)
            }
            XCTAssertGreaterThan(posts.count, 0)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: timeout)
    }
    
    func testGetHotels() {
        let expectation = self.expectation(description: "GetHotels")
        
        APIClient.getHotels(cityId: "3") { (hotelcity, message, error) in
            
            if let checkMsg = message  {
                print(checkMsg)
            }
            
            XCTAssertGreaterThan(hotelcity.count, 0)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: timeout)
    }

    
    func testRegisterUsers() {
        let expectation = self.expectation(description: "RegisterUsers")
        
        APIClient.register(name: "jerry", email: "jerry@test.com", password: "123456", address: "Malir", phone: "9237849994") { (errors, message, user) in
            
            if let checkMsg = message  {
                print(checkMsg)
            }
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: timeout)
    }

    func testLoginUser() {
        let expectation = self.expectation(description: "LoginUser")
    
        APIClient.login(email: "jerry@test.com", password: "12s3456") { (errors, message, user) in
            
            if let checkMsg = message  {
                print(checkMsg)
            }
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: timeout)
    }
}
